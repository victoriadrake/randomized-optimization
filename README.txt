Randomized Optimization

This repository contains data sets and Jupyter notebooks for exploring random search algorithms:

- A “small” data set, Titanic: ./titanic/train.csv.zip
    - Original source: https://www.kaggle.com/c/titanic/data
- A larger data set, IEEE-CIS Fraud Detection: ./fraud/train_transaction.csv.zip
    - Original source: https://www.kaggle.com/c/ieee-fraud-detection/data
- Jupyter notebooks:
    - A comparison of algorithms on the Four Peaks problem: four_peaks.ipynb
    - A comparison of algorithms on the OneMax problem: onemax.ipynb
    - A comparison of algorithms on the Knapsack Problem: knapsack.ipynb
    - Exploration of neural network weights optimization on the IEEE-CIS Fraud Detection data set: nnopts_fraud.ipynb
    - Exploration of neural network weights optimization on the Titanic data set: nnopts_titanic.ipynb

To run the notebooks:

1. Ensure Jupyter is installed. See: https://jupyter.org/install
2. Install dependencies from requirements.txt or the Pipfile using your choice of Python environment
3. Launch Jupyter with the terminal command: jupyter notebook
